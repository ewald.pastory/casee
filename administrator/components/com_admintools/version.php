<?php
/**
 * @package   admintools
 * @copyright Copyright (c)2010-2022 Nicholas K. Dionysopoulos / Akeeba Ltd
 * @license   GNU General Public License version 3, or later
 */

// Protect from unauthorized access
defined('_JEXEC') or die;

define('ADMINTOOLS_VERSION', '7.1.9');
define('ADMINTOOLS_DATE', '2022-07-11');
define('ADMINTOOLS_PRO','0');